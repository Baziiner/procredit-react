import React, { useEffect, useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { Button } from "antd";
import {
  getUsers,
  deleteUser,
  promoteUser,
  demoteUser,
  removeUserMember,
} from "../../../_actions/user_actions";
import MemberForm from "../../MemberForm";
import UserEdit from "./UserEdit";
import { Link } from "react-router-dom";
import "./AdminPage.scss";

const AdminPage = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  const getUserList = useCallback(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const postRemoveMember = useCallback(
    (id, memberId) => {
      dispatch(removeUserMember(id, memberId)).then((response) => {
        if (response.payload.data.postSuccess) {
          alert(response.payload.data.message);
          getUserList();
        } else {
          alert("Something went wrong.");
        }
      });
    },
    [dispatch]
  );

  const handleDeleteUser = useCallback(
    (id) => {
      dispatch(deleteUser(id)).then((response) => {
        if (response.payload.postSuccess) {
          alert(response.payload.message);
        } else {
          alert(response.payload.error);
        }
      });
      getUserList();
    },
    [dispatch]
  );

  const handleUserPromote = useCallback(
    (id) => {
      dispatch(promoteUser(id)).then((response) => {
        if (response.payload.data.postSuccess) {
          alert(response.payload.data.message);
          getUserList();
        } else {
          alert(response.payload.error);
        }
      });
    },
    [dispatch]
  );

  const handleUserDemote = useCallback(
    (id) => {
      dispatch(demoteUser(id)).then((response) => {
        if (response.payload.data.postSuccess) {
          alert(response.payload.data.message);
          getUserList();
        } else {
          alert(response.payload.error);
        }
      });
    },
    [dispatch]
  );

  useEffect(() => {
    getUserList();
  }, []);

  const [view, setView] = useState(0);
  const [selectedIndex, setIndex] = useState({
    familyIndex: -1,
    memberIndex: -1,
  });

  const handleEdit = (index) => {
    setView(2);
    setIndex(index);
  };

  const handleAdd = (index) => {
    setView(1);
    setIndex(index);
  };

  const handleUserEdit = (index) => {
    setView(3);
    setIndex(index);
  };

  return (
    <div className="AdminPage">
      {view === 0 && (
        <>
          <Button type="primary" className="register-button">
            <Link to="/register">Register a new user</Link>
          </Button>
          {user.userData && !user.userData.adminData ? (
            <div>Loading...</div>
          ) : (
            <>
              {user.userData.adminData.map((data, index) => (
                <div className="user-list-custom" key={index}>
                  <div className="user-status">
                    <Button
                      type="default"
                      className="member-button"
                      onClick={() => handleAdd({ userIndex: index })}
                    >
                      Add New Member
                    </Button>
                    {data._id === user.userData._id ? (
                      <span>Currently logged in</span>
                    ) : (
                      <>
                        <Button
                          onClick={() => handleUserEdit({ userIndex: index })}
                        >
                          Edit User
                        </Button>
                        <Button
                          type="danger"
                          onClick={() => handleDeleteUser(data._id)}
                        >
                          Delete User
                        </Button>
                        {data.role === 0 ? (
                          <Button
                            type="primary"
                            onClick={() => handleUserPromote(data._id)}
                          >
                            Give Admin Privilages
                          </Button>
                        ) : (
                          <Button
                            type="primary"
                            onClick={() => handleUserDemote(data._id)}
                          >
                            Remove Admin Privilages
                          </Button>
                        )}
                      </>
                    )}
                  </div>
                  <div className="user-list-titles">
                    <div className="title-main -id">ID</div>
                    <div className="title -id">{data._id}</div>
                    <div className="title-main -name">Name</div>
                    <div className="title -name">{data.name}</div>
                    <div className="title-main -lname">Last Name</div>
                    <div className="title -lname">{data.lastName}</div>
                    <div className="title-main -email">Email</div>
                    <div className="title -email">{data.email}</div>
                  </div>
                  <div className="user-family-list">
                    {data.familyMember.map((familyData, i) => (
                      <div className="list-items" key={i}>
                        <div className="list-item -name">
                          <span>First Name: </span> {familyData.firstName}
                        </div>
                        <div className="list-item -lName">
                          <span>Last Name: </span> {familyData.lastName}
                        </div>
                        <div className="list-item -birthday">
                          <span>Birthday:</span>{" "}
                          {moment(familyData.birthday).format("MMM Do YYYY")}
                        </div>
                        <div className="list-item -status">
                          <span>Family member:</span> {familyData.memberStatus}
                        </div>
                        <div className="list-item -occupation">
                          <span>Occupation:</span> {familyData.occupation}
                        </div>
                        <div className="list-item -createdAt">
                          <span>Created At:</span>{" "}
                          {moment(familyData.createdAt).format("MMM Do YYYY")}
                        </div>
                        <div className="list-item -editedAt">
                          <span>Edited At:</span>{" "}
                          {moment(familyData.editedAt).format("MMM Do YYYY")}
                        </div>
                        <div className="list-item -editedBy">
                          <span>Last Edit By:</span> {familyData.editedBy}
                        </div>
                        <div className="list-item -editedBy">
                          <Button
                            type="primary"
                            onClick={() =>
                              handleEdit({ userIndex: index, memberIndex: i })
                            }
                          >
                            Edit
                          </Button>
                          <Button
                            type="secondary"
                            onClick={() =>
                              postRemoveMember(data._id, familyData._id)
                            }
                          >
                            Delete
                          </Button>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </>
          )}
        </>
      )}
      {(view === 1 || view === 2) && (
        <MemberForm
          view={view}
          data={
            view !== 1 &&
            user.userData.adminData[selectedIndex.userIndex].familyMember[
              selectedIndex.memberIndex
            ]
          }
          setView={setView}
          getMembersList={getUserList}
          indexes={selectedIndex}
          isAdmin
        />
      )}
      {view === 3 && (
        <UserEdit
          view={view}
          data={view !== 1 && user.userData.adminData[selectedIndex.userIndex]}
          setView={setView}
          getMembersList={getUserList}
        />
      )}
    </div>
  );
};

export default AdminPage;
