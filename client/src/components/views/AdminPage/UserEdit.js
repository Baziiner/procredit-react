import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form, Input, Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { editUserProfile } from "../../../_actions/user_actions";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const UserEdit = ({ data, view, setView, getMembersList }) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const updateOnSuccess = (res) => {
    alert(res);
    setView(0);
    getMembersList();
  };

  return (
    <div className="UserEdit">
      {!data ? (
        <span>Loading...</span>
      ) : (
        <Formik
          initialValues={{
            name: data.name,
            lName: data.lastName,
            email: data.email,
          }}
          validationSchema={Yup.object().shape({
            name: Yup.string().required("Name is required"),
            lName: Yup.string().required("Last Name is required"),
            email: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
          })}
          onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
              let dataToSubmit = {
                name: values.name,
                lName: values.lName,
                _id: data._id,
              };

              dispatch(editUserProfile(dataToSubmit)).then((response) => {
                if (response.payload.postSuccess) {
                  updateOnSuccess(response.payload.message);
                } else {
                  alert(response.payload.error);
                }
              });

              setSubmitting(false);
            }, 500);
          }}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
            } = props;
            return (
              <div className="app">
                <h2>EDIT USER DETAILS</h2>
                <Form
                  style={{ minWidth: "375px" }}
                  {...formItemLayout}
                  onSubmit={handleSubmit}
                >
                  <Form.Item required label="First Name">
                    <Input
                      id="name"
                      placeholder="Enter name"
                      type="text"
                      value={values.name}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.name && touched.name
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.name && touched.name && (
                      <div className="input-feedback">{errors.name}</div>
                    )}
                  </Form.Item>

                  <Form.Item required label="Last Name">
                    <Input
                      id="lName"
                      placeholder="Enter member's Last Name"
                      type="text"
                      value={values.lName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.lName && touched.lName
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.lName && touched.lName && (
                      <div className="input-feedback">{errors.lName}</div>
                    )}
                  </Form.Item>

                  <Form.Item
                    required
                    label="Email"
                    hasFeedback
                    validateStatus={
                      errors.email && touched.email ? "error" : "success"
                    }
                  >
                    <Input
                      id="email"
                      placeholder="Enter your Email"
                      type="email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      disabled
                      className={
                        errors.email && touched.email
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.email && touched.email && (
                      <div className="input-feedback">{errors.email}</div>
                    )}
                  </Form.Item>

                  <Form.Item {...tailFormItemLayout}>
                    <Button
                      onClick={handleSubmit}
                      type="primary"
                      disabled={isSubmitting}
                      className="form-button"
                    >
                      Submit
                    </Button>
                    <Button
                      onClick={() => setView(0)}
                      type="secondary"
                      disabled={isSubmitting}
                      className="form-button"
                    >
                      Go Back
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            );
          }}
        </Formik>
      )}
    </div>
  );
};

export default UserEdit;
