import React from "react";
import FamilyList from "../../FamilyList";
import LandingNoAuth from "../../LandingNoAuth";
import { useSelector } from "react-redux";

function LandingPage() {
  const user = useSelector((state) => state.user);

  return (
    <>
      <div className="app">
        {user.userData && !user.userData.isAuth ? (
          <LandingNoAuth />
        ) : (
          <FamilyList />
        )}
      </div>
    </>
  );
}

export default LandingPage;
