import React from "react";
import { Menu } from "antd";
import { useSelector } from "react-redux";

function LeftMenu(props) {
  const user = useSelector((state) => state.user);

  return (
    <Menu mode={props.mode}>
      <Menu.Item>
        <a href="/">Home</a>
      </Menu.Item>
      {user.userData && user.userData.isAdmin && (
        <Menu.Item>
          <a href="/Admin">Admin</a>
        </Menu.Item>
      )}
    </Menu>
  );
}

export default LeftMenu;
