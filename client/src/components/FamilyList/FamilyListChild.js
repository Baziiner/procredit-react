import React from "react";
import moment from "moment";
import { Button } from "antd";

const FamilyListChild = ({ data, removeMember, handleEdit }) => {
  return (
    <div className="family-list-children">
      <div className="list -name">{data.firstName}</div>
      <div className="list -lname">{data.lastName}</div>
      <div className="list -age">
        {moment(data.birthday).fromNow().replace(/\D/g, "")}
      </div>
      <div className="list -status">{data.memberStatus}</div>
      <div className="list -Occupation">{data.occupation}</div>
      <div className="list -Date">
        {moment(data.editedAt).format("MMMM Do YYYY") ||
          moment(data.createdAt).format("MMMM Do YYYY")}
      </div>
      <div className="list -Author">{data.editedBy}</div>
      <div className="list -actions">
        <Button type="primary" onClick={() => handleEdit()}>
          Edit
        </Button>
        <Button type="secondary" onClick={() => removeMember(data._id)}>
          Delete
        </Button>
      </div>
    </div>
  );
};

export default FamilyListChild;
