import React, { useEffect, useState, useCallback } from "react";
import { Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { getMembers, removeMember } from "../../_actions/user_actions";
import "./FamilyList.scss";
import FamilyListChild from "./FamilyListChild";
import MemberForm from "../MemberForm";

const FamilyList = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  const getMembersList = useCallback(() => {
    dispatch(getMembers());
  }, [dispatch]);

  const postRemoveMember = useCallback(
    (id) => {
      dispatch(removeMember(id)).then((response) => {
        if (response.payload.postSuccess) {
          alert(response.payload.message);
          setView(0);
        } else {
          alert("Something went wrong.");
        }
      });
    },
    [dispatch]
  );

  useEffect(() => {
    getMembersList();
  }, []);

  const [view, setView] = useState(0);
  const [selectedIndex, setIndex] = useState(-1);

  const handleEdit = (index) => {
    setView(2);
    setIndex(index);
  };

  return (
    <div className="FamilyList">
      {view === 0 ? (
        <>
          <div className="list-top">
            <h3 className="list-title">My Family Members</h3>
            <Button type="primary" onClick={() => setView(1)}>
              Add New Family Member
            </Button>
          </div>
          <div className="family-list-custom">
            <div className="family-list-titles">
              <div className="title -name">Name</div>
              <div className="title -lname">Last Name</div>
              <div className="title -age">Age</div>
              <div className="title -status">Family Status</div>
              <div className="title -Occupation">Occupation</div>
              <div className="title -Date">Date</div>
              <div className="title -Author">Last Edit</div>
              <div className="title -actions">Actions</div>
            </div>
            {user.userData && !user.userData.familyMember ? (
              <div>Loading...</div>
            ) : (
              user.userData.familyMember.map((data, index) => (
                <FamilyListChild
                  data={data}
                  key={index}
                  removeMember={postRemoveMember}
                  handleEdit={() => handleEdit(index)}
                />
              ))
            )}
          </div>
        </>
      ) : (
        <MemberForm
          view={view}
          data={user.userData.familyMember[selectedIndex]}
          setView={setView}
          getMembersList={getMembersList}
        />
      )}
    </div>
  );
};

export default FamilyList;
