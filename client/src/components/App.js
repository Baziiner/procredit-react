import React, { Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import Auth from "../hoc/auth";
import LandingPage from "./views/LandingPage";
import LoginPage from "./views/LoginPage";
import RegisterPage from "./views/RegisterPage";
import AdminPage from "./views/AdminPage";
import Profile from "./views/Profile";
import NavBar from "./views/NavBar";
import Footer from "./views/Footer";

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <NavBar />
      <div className="main-content">
        <Switch>
          <Route exact path="/" component={Auth(LandingPage, null)} />
          <Route exact path="/login" component={Auth(LoginPage, false)} />
          <Route exact path="/register" component={Auth(RegisterPage, null)} />
          <Route exact path="/admin" component={Auth(AdminPage, true)} />
          <Route exact path="/profile" component={Auth(Profile, true)} />
        </Switch>
      </div>
      <Footer />
    </Suspense>
  );
}

export default App;
