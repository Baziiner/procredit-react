import React from "react";
import "./LandingNoAuth.scss";

const LandingNoAuth = () => {
  return (
    <div className="LandingNoAuth">
      <h1>Welcome. Don't forget to authorize to continue</h1>
      <div className="button-wrapper">
        <a className="button -register" href="/register">
          Register
        </a>
        <a className="button -signin" href="/login">
          Sign-in
        </a>
      </div>
    </div>
  );
};

export default LandingNoAuth;
