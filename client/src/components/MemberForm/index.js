import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import moment from "moment";
import { Form, Input, Button, DatePicker } from "antd";
import {
  addMember,
  editMember,
  addUserMember,
  editUserMember,
} from "../../_actions/user_actions";
import { useDispatch, useSelector } from "react-redux";
import "./MemberForm.scss";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const MemberForm = ({
  view,
  setView,
  getMembersList,
  data,
  isAdmin,
  indexes,
}) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const updateOnSuccess = (res) => {
    alert(res);
    setView(0);
    getMembersList();
  };

  const formikValues = () => {
    let initVal = {};

    if (view === 1) {
      initVal = {
        name: "",
        lName: "",
        birthDay: 0,
        memberStatus: "",
        occupation: "",
        createdAt: "",
        editedBy: "",
      };
    } else {
      initVal = {
        name: data.firstName,
        lName: data.lastName,
        birthDay: data.birthday,
        memberStatus: data.memberStatus,
        occupation: data.occupation,
        editedAt: Date.now(),
        editedBy:
          user.userData && !user.userData.isAuth
            ? null
            : `${user.userData.name} ${user.userData.lastName}`,
      };
    }

    return initVal;
  };
  const handleViewSubmit = (dataToSubmit) => {
    if (!isAdmin) {
      if (view === 1) {
        dispatch(addMember(dataToSubmit)).then((response) => {
          if (response.payload.postSuccess) {
            updateOnSuccess(response.payload.message);
          } else {
            alert("Something went wrong.");
          }
        });
      } else {
        dispatch(editMember(dataToSubmit, data._id)).then((response) => {
          if (response.payload.postSuccess) {
            updateOnSuccess(response.payload.message);
          } else {
            alert("Something went wrong.");
          }
        });
      }
    } else {
      if (view === 1) {
        dispatch(addUserMember(dataToSubmit)).then((response) => {
          if (response.payload.postSuccess) {
            updateOnSuccess(response.payload.message);
          } else {
            alert("Something went wrong.");
          }
        });
      } else {
        dispatch(
          editUserMember(
            dataToSubmit,
            user.userData.adminData[indexes.userIndex]._id,
            data._id
          )
        ).then((response) => {
          if (response.payload.postSuccess) {
            updateOnSuccess(response.payload.message);
          } else {
            alert("Something went wrong.");
          }
        });
      }
    }
  };

  return (
    <div className="MemberForm">
      <Formik
        initialValues={formikValues()}
        validationSchema={Yup.object().shape({
          name: Yup.string().required("Name is required"),
          lName: Yup.string().required("Last Name is required"),
          memberStatus: Yup.string().required("Member Status is required"),
          occupation: Yup.string().required("Occupation Status is required"),
          birthDay: Yup.date().required("Birthday is required"),
        })}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            let dataToSubmit = {
              name: values.name,
              lName: values.lName,
              birthDay: values.birthDay,
              memberStatus: values.memberStatus,
              occupation: values.occupation,
              editedAt: Date.now(),
              createdAt: view === 1 ? Date.now() : data.createdAt,
              editedBy:
                user.userData && !user.userData.isAuth
                  ? null
                  : `${user.userData.name} ${user.userData.lastName}`,
              _id:
                user.userData && !user.userData.isAuth
                  ? null
                  : !isAdmin
                  ? user.userData._id
                  : user.userData.adminData[indexes.userIndex]._id,
            };

            handleViewSubmit(dataToSubmit);

            setSubmitting(false);
          }, 500);
        }}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
          } = props;
          return (
            <div className="app">
              <h2>{view === 1 ? "Add" : "Edit"} Family Member</h2>
              <Form
                style={{ minWidth: "375px" }}
                {...formItemLayout}
                onSubmit={handleSubmit}
              >
                <Form.Item required label="Name">
                  <Input
                    id="name"
                    placeholder="Enter member's name"
                    type="text"
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.name && touched.name
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  {errors.name && touched.name && (
                    <div className="input-feedback">{errors.name}</div>
                  )}
                </Form.Item>

                <Form.Item required label="Last Name">
                  <Input
                    id="lName"
                    placeholder="Enter member's Last Name"
                    type="text"
                    value={values.lName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.lName && touched.lName
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  {errors.lName && touched.lName && (
                    <div className="input-feedback">{errors.lName}</div>
                  )}
                </Form.Item>

                <Form.Item
                  required
                  label="Member's Status"
                  hasFeedback
                  validateStatus={
                    errors.memberStatus && touched.memberStatus
                      ? "error"
                      : "success"
                  }
                >
                  <Input
                    id="memberStatus"
                    placeholder="Enter member's status"
                    type="text"
                    value={values.memberStatus}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.memberStatus && touched.memberStatus
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  {errors.memberStatus && touched.memberStatus && (
                    <div className="input-feedback">{errors.memberStatus}</div>
                  )}
                </Form.Item>

                <Form.Item
                  required
                  label="Occupation"
                  hasFeedback
                  validateStatus={
                    errors.occupation && touched.occupation
                      ? "error"
                      : "success"
                  }
                >
                  <Input
                    id="occupation"
                    placeholder="Enter occupation"
                    type="text"
                    value={values.occupation}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.occupation && touched.occupation
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  {errors.occupation && touched.occupation && (
                    <div className="input-feedback">{errors.occupation}</div>
                  )}
                </Form.Item>

                <Form.Item required label="Date" hasFeedback>
                  <DatePicker
                    onChange={(date, dateString) => {
                      values.birthDay = moment(date);
                    }}
                    defaultValue={moment(values.birthDay)}
                  />
                  {errors.DatePicker && touched.DatePicker && (
                    <div className="input-feedback">{errors.DatePicker}</div>
                  )}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                  <Button
                    onClick={handleSubmit}
                    type="primary"
                    disabled={isSubmitting}
                    className="form-button"
                  >
                    Submit
                  </Button>
                  <Button
                    onClick={() => setView(0)}
                    type="secondary"
                    disabled={isSubmitting}
                    className="form-button"
                  >
                    Go Back
                  </Button>
                </Form.Item>
              </Form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default MemberForm;
