import axios from "axios";
import {
  LOGIN_USER,
  REGISTER_USER,
  AUTH_USER,
  LOGOUT_USER,
  ADD_MEMBER,
  GET_MEMBERS,
  REMOVE_MEMBER,
  EDIT_MEMBER,
  GET_USERS,
  DELETE_USER,
  PROMOTE_USER,
  REMOVE_USER_MEMBER,
  ADD_USER_MEMBER,
  EDIT_USER_MEMBER,
  EDIT_PROFILE,
} from "./types";
import { USER_SERVER } from "../components/Config.js";

export function registerUser(dataToSubmit) {
  const request = axios
    .post(`${USER_SERVER}/register`, dataToSubmit)
    .then((response) => response.data);

  return {
    type: REGISTER_USER,
    payload: request,
  };
}

export function loginUser(dataToSubmit) {
  const request = axios
    .post(`${USER_SERVER}/login`, dataToSubmit)
    .then((response) => response.data);

  return {
    type: LOGIN_USER,
    payload: request,
  };
}

export function addMember(dataToSubmit) {
  const request = axios
    .post(`${USER_SERVER}/add-member`, dataToSubmit)
    .then((response) => response.data);

  return {
    type: ADD_MEMBER,
    payload: request,
  };
}

export function removeMember(id) {
  const request = axios
    .post(`${USER_SERVER}/remove-member`, { _id: id })
    .then((response) => response.data);

  return {
    type: REMOVE_MEMBER,
    payload: request,
  };
}

export function editMember(dataToSubmit, id) {
  const request = axios
    .post(`${USER_SERVER}/edit-member`, { dataToSubmit, _id: id })
    .then((response) => response.data);
  return {
    type: EDIT_MEMBER,
    payload: request,
  };
}

export function getMembers() {
  const request = axios
    .get(`${USER_SERVER}/members`)
    .then((response) => response.data);

  return {
    type: GET_MEMBERS,
    payload: request,
  };
}

export function getUsers() {
  const request = axios
    .get(`${USER_SERVER}/admin/get-users`)
    .then((response) => response.data);

  return {
    type: GET_USERS,
    payload: request,
  };
}

export function deleteUser(id) {
  const request = axios
    .delete(`${USER_SERVER}/admin/remove-user`, { data: { _id: id } })
    .then((response) => response.data);

  return {
    type: DELETE_USER,
    payload: request,
  };
}

export function promoteUser(id) {
  const request = axios.post(`${USER_SERVER}/admin/set-admin`, {
    _id: id,
  });

  return {
    type: PROMOTE_USER,
    payload: request,
  };
}

export function removeUserMember(id, memberId) {
  const request = axios.post(`${USER_SERVER}/admin/remove-member`, {
    _id: id,
    member_id: memberId,
  });

  return {
    type: REMOVE_USER_MEMBER,
    payload: request,
  };
}

export function addUserMember(dataToSubmit) {
  const request = axios
    .post(`${USER_SERVER}/admin/add-member`, dataToSubmit)
    .then((response) => response.data);

  return {
    type: ADD_USER_MEMBER,
    payload: request,
  };
}

export function editUserMember(dataToSubmit, id, memberId) {
  const request = axios
    .post(`${USER_SERVER}/admin/edit-member`, {
      dataToSubmit,
      _id: id,
      member_id: memberId,
    })
    .then((response) => response.data);
  return {
    type: EDIT_USER_MEMBER,
    payload: request,
  };
}

export function demoteUser(id) {
  const request = axios.post(`${USER_SERVER}/admin/remove-admin`, {
    _id: id,
  });

  return {
    type: PROMOTE_USER,
    payload: request,
  };
}

export function editProfile(dataToSubmit) {
  const request = axios
    .post(`${USER_SERVER}/edit-profile`, dataToSubmit)
    .then((response) => response.data);

  return {
    type: EDIT_PROFILE,
    payload: request,
  };
}

export function editUserProfile(dataToSubmit) {
  const request = axios
    .post(`${USER_SERVER}/admin/edit-profile`, dataToSubmit)
    .then((response) => response.data);
  return {
    type: EDIT_USER_MEMBER,
    payload: request,
  };
}

export function auth() {
  const request = axios
    .get(`${USER_SERVER}/auth`)
    .then((response) => response.data);

  return {
    type: AUTH_USER,
    payload: request,
  };
}

export function logoutUser() {
  const request = axios
    .get(`${USER_SERVER}/logout`)
    .then((response) => response.data);

  return {
    type: LOGOUT_USER,
    payload: request,
  };
}
