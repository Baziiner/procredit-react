import {
  LOGIN_USER,
  REGISTER_USER,
  AUTH_USER,
  LOGOUT_USER,
  GET_MEMBERS,
  REMOVE_MEMBER,
  GET_USERS,
  EDIT_PROFILE,
} from "../_actions/types";

const initialState = {
  userData: {
    isAuth: false,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REGISTER_USER:
      return { ...state, register: action.payload };
    case LOGIN_USER:
      return { ...state, loginSucces: action.payload };
    case AUTH_USER:
      return { ...state, userData: action.payload };
    case LOGOUT_USER:
      return { ...state };
    case GET_MEMBERS:
      return {
        ...state,
        userData: {
          ...state.userData,
          familyMember: action.payload.familyMembers,
        },
      };
    case REMOVE_MEMBER:
      return {
        ...state,
        userData: {
          ...state.userData,
          familyMember: action.payload.data,
        },
      };
    case GET_USERS:
      return {
        ...state,
        userData: {
          ...state.userData,
          adminData: action.payload,
        },
      };
    case EDIT_PROFILE:
      return {
        ...state,
        userData: {
          ...state.userData,
          name: action.payload.data.name,
          lastName: action.payload.data.lastName,
        },
      };
    default:
      return state;
  }
}
