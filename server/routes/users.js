const express = require("express");
const router = express.Router();
var mongoose = require("mongoose");
const { User } = require("../models/User");
const { auth } = require("../middleware/auth");

router.get("/auth", auth, (req, res) => {
  res.status(200).json({
    _id: req.user._id,
    isAdmin: req.user.role === 0 ? false : true,
    isAuth: true,
    email: req.user.email,
    name: req.user.name,
    lastName: req.user.lastName,
    role: req.user.role,
  });
});

router.post("/register", (req, res) => {
  const user = new User(req.body);
  user.save((err, doc) => {
    if (err) return res.json({ success: false, err });
    return res.status(200).json({
      success: true,
    });
  });
});

router.post("/login", (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (!user)
      return res.json({
        loginSuccess: false,
        message: "Auth failed, email not found",
      });

    user.comparePassword(req.body.password, (err, isMatch) => {
      if (!isMatch)
        return res.json({ loginSuccess: false, message: "Wrong password" });

      user.generateToken((err, user) => {
        if (err) return res.status(400).send(err);
        res.cookie("w_authExp", user.tokenExp);
        res.cookie("w_auth", user.token).status(200).json({
          loginSuccess: true,
          userId: user._id,
        });
      });
    });
  });
});

router.get("/members", auth, (req, res) => {
  const user = req.user;

  return res.json({
    familyMembers: user.familyMember,
  });
});

router.post("/add-member", auth, (req, res) => {
  User.findById(req.user._id, (err, user) => {
    if (!user && req.body._id != req.user._id)
      return res.json({
        addSuccess: false,
        message: "Error couldn't add member",
      });

    const name = req.body.name;
    const lName = req.body.lName;
    const birthDay = req.body.birthDay;
    const memberStatus = req.body.memberStatus;
    const createdAt = Date.now();
    const editedAt = req.body.editedAt;
    const editedBy = req.body.editedBy;
    const occupation = req.body.occupation;

    user.familyMember.push({
      firstName: name,
      lastName: lName,
      birthday: birthDay,
      memberStatus: memberStatus,
      occupation: occupation,
      createdAt: createdAt,
      editedAt: editedAt,
      editedBy: editedBy,
    });

    user.save();

    return res.json({ postSuccess: true, message: "Added to Members" });
  });
});

router.post("/remove-member", auth, (req, res) => {
  User.findById(req.user._id, (err, user) => {
    if (!user)
      return res.json({
        removeSuccess: false,
        message: "Error couldn't remove member",
      });

    user.familyMember.pull(req.body._id);
    user.save();

    return res.json({
      postSuccess: true,
      message: "Member Successfuly removed",
      data: user.familyMember,
    });
  });
});

router.post("/edit-member", auth, (req, res) => {
  User.findById(req.user._id, (err, user) => {
    if (!user)
      return res.json({
        editSuccess: false,
        message: "Error couldn't edit member",
      });

    const name = req.body.dataToSubmit.name;
    const lName = req.body.dataToSubmit.lName;
    const birthDay = req.body.dataToSubmit.birthDay;
    const memberStatus = req.body.dataToSubmit.memberStatus;
    const createdAt = Date.now();
    const editedAt = Date.now();
    const occupation = req.body.dataToSubmit.occupation;
    const editedBy = req.body.dataToSubmit.editedBy;

    //this method is incorrect and should be refactored. had issues with mongoose.update not updating the array. this is a temporary solution
    user.familyMember.pull(req.body._id);
    user.familyMember.push({
      firstName: name,
      lastName: lName,
      birthday: birthDay,
      memberStatus: memberStatus,
      occupation: occupation,
      createdAt: createdAt,
      editedAt: editedAt,
      editedBy: editedBy,
    });

    user.save();

    return res.json({
      postSuccess: true,
      message: "Member Successfuly edited",
      data: user.familyMember,
    });
  });
});

router.get("/admin/get-users", auth, (req, res) => {
  if (req.user.role !== 0) {
    User.find({}, (err, users) => {
      let userMap = [];
      users.forEach((user) => {
        userMap.push({
          _id: user.id,
          name: user.name,
          lastName: user.lastName,
          email: user.email,
          familyMember: user.familyMember,
          role: user.role,
        });
      });
      res.send(userMap);
    });
  }
});

router.post("/admin/add-member", auth, (req, res) => {
  if (req.user.role !== 0) {
    User.findById(req.body._id, (err, user) => {
      if (!user)
        return res.json({
          addSuccess: false,
          message: "Error couldn't add member",
        });

      const name = req.body.name;
      const lName = req.body.lName;
      const birthDay = req.body.birthDay;
      const memberStatus = req.body.memberStatus;
      const createdAt = Date.now();
      const editedAt = req.body.editedAt;
      const editedBy = req.body.editedBy;
      const occupation = req.body.occupation;

      user.familyMember.push({
        firstName: name,
        lastName: lName,
        birthday: birthDay,
        memberStatus: memberStatus,
        occupation: occupation,
        createdAt: createdAt,
        editedAt: editedAt,
        editedBy: editedBy,
      });

      user.save();

      return res.json({ postSuccess: true, message: "Added to Members" });
    });
  }
});

router.post("/admin/remove-member", auth, (req, res) => {
  if (req.user.role !== 0) {
    User.findById(req.body._id, (err, user) => {
      if (!user)
        return res.json({
          removeSuccess: false,
          message: "Error couldn't remove member",
        });

      user.familyMember.pull(req.body.member_id);
      user.save();

      return res.json({
        postSuccess: true,
        message: "Member Successfuly removed",
        data: user.familyMember,
      });
    });
  }
});

router.post("/admin/edit-member", auth, (req, res) => {
  User.findById(req.body._id, (err, user) => {
    if (!user)
      return res.json({
        editSuccess: false,
        message: "Error couldn't edit member",
      });

    const name = req.body.dataToSubmit.name;
    const lName = req.body.dataToSubmit.lName;
    const birthDay = req.body.dataToSubmit.birthDay;
    const memberStatus = req.body.dataToSubmit.memberStatus;
    const createdAt = Date.now();
    const editedAt = Date.now();
    const occupation = req.body.dataToSubmit.occupation;
    const editedBy = req.body.dataToSubmit.editedBy;

    //this method is incorrect and should be refactored. had issues with mongoose.update not updating the array. this is a temporary solution
    user.familyMember.pull(req.body.member_id);
    user.familyMember.push({
      firstName: name,
      lastName: lName,
      birthday: birthDay,
      memberStatus: memberStatus,
      occupation: occupation,
      createdAt: createdAt,
      editedAt: editedAt,
      editedBy: editedBy,
    });

    user.save();

    return res.json({
      postSuccess: true,
      message: "Member Successfuly edited",
      data: user.familyMember,
    });
  });
});

router.delete("/admin/remove-user", auth, (req, res) => {
  if (req.user.role !== 0 && req.user._id !== req.body._id) {
    User.findByIdAndDelete(req.body._id, (err, user) => {
      if (err) {
        return res.json({ error: err });
      } else {
        return res.json({
          postSuccess: true,
          message: `ID: '${req.body._id}' Deleted Succesfully`,
        });
      }
    });
  }
});

router.post("/admin/set-admin", auth, (req, res) => {
  if (req.user.role !== 0 && req.user._id !== req.body._id) {
    User.findOneAndUpdate({ _id: req.body._id }, { role: 1 }, (err, doc) => {
      if (err) {
        return res.json({ error: err });
      } else {
        doc.role = 1;
        doc.save();
        return res.json({
          postSuccess: true,
          message: `ID: ;${req.body._id}' Set As Admin Succesfully`,
        });
      }
    });
  }
});

router.post("/admin/remove-admin", auth, (req, res) => {
  if (req.user.role !== 0 && req.user._id !== req.body._id) {
    User.findOneAndUpdate({ _id: req.body._id }, { role: 0 }, (err, doc) => {
      if (err) {
        return res.json({ error: err });
      } else {
        doc.role = 0;
        doc.save();
        return res.json({
          postSuccess: true,
          message: `ID: '${req.body._id}' Unset as Admin Succesfully`,
        });
      }
    });
  }
});

router.post("/edit-profile", auth, (req, res) => {
  User.findOneAndUpdate(
    { _id: req.user._id },
    { name: req.body.name, lastName: req.body.lName },
    (err, doc) => {
      if (err) {
        return res.json({ error: err });
      } else {
        doc.name = req.body.name;
        doc.lastName = req.body.lName;
        doc.save();
        return res.json({
          postSuccess: true,
          message: `Profile Updated Successfully`,
          data: {
            name: req.body.name,
            lastName: req.body.lName,
          },
        });
      }
    }
  );
});

router.post("/admin/edit-profile", auth, (req, res) => {
  if (req.user.role !== 0 && req.user._id !== req.body._id) {
    User.findOneAndUpdate(
      { _id: req.body._id },
      { name: req.body.name, lastName: req.body.lName },
      (err, doc) => {
        if (err) {
          return res.json({ error: err });
        } else {
          doc.name = req.body.name;
          doc.lastName = req.body.lName;
          doc.save();
          return res.json({
            postSuccess: true,
            message: `Profile Updated Successfully`,
          });
        }
      }
    );
  }
});

router.get("/logout", auth, (req, res) => {
  User.findOneAndUpdate(
    { _id: req.user._id },
    { token: "", tokenExp: "" },
    (err, doc) => {
      if (err) return res.json({ success: false, err });
      return res.status(200).send({
        success: true,
      });
    }
  );
});

module.exports = router;
